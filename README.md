# AD9249 16-Channel 14 bits 65MSPS ADC interface 

This IP is part of the CARiBOu firmware suite and designed to interface with the AD9249 ADC mounted on the CaR board v1.x.  

## AD9249 ADC Hardware Configuration

### Features
The data sheet of the component can be found [here](https://www.analog.com/media/en/technical-documentation/data-sheets/AD9249.pdf).

* 16 Channel (differential), 14 bit sampling
* Sampling rate up to 65 Msps
* Input clock : 65 MHz to 520 MHz with internal clock divider (ratio 1 to 8)
* SPI programmation interface
* Continuous sampling, output on 16 LVDS differential link, with Frame (byte) and data clock signal  


### Clocking

On the CAR Board v1.x, the ADC differential input clock is provided via the [Si5345 Clock generator and Jitter cleaner](https://www.silabs.com/documents/public/data-sheets/Si5345-44-42-D-DataSheet.pdf) on board. The ADC clock is provided on output 0 of the si5345. To operate the ADC, make sure the Si5345 has been program to provide a clock on this output, with a frequency between 65 and 520 MHz. Data are provided on 16 outputs with 2 associated clocks, the DATA_CLK and FRAME_CLK. The Frame clock determine each sample in the data stream, while the data clock os synchronize with the data output. The data for each sample is provided on both edge of the clock. Therefore, FRAME_CLK has a frequency of 1/7 of the DATA_CLK frequency. and equal to the input frequency divided by the input clock divider ratio selectable by SPI configuration of the IC between 1 and 8.



### Configuration of IC

The configuration of the AD9249 is done via the SPI interface. The details of the registers are provided in the [documentation](https://www.silabs.com/documents/public/data-sheets/Si5345-44-42-D-DataSheet.pdf). If using the peary software, the configuration code is provided. Otherwise it is left to the user to program the IC with the required parameters. Default settings should be sufficient to operate with this IP. However, make sure the following register are set correctly or modified following you design.

| Register | function | Value | Motivation |
| -------- | -------- | ----- | ---------- |
| 0x08| power mode | 0x00 | Set chip to running mode |
| 0x0B| clock divider | 0x00 | No clock division,if used, attention must be paid to IP internal clocking (se IP section) |
| 0x0D| output mode | 0x01 | LVDS-ANSI |
| 0x15| output adjust | 0x00 | No termination |
| 0x21| serial output data control| 0x41 | DDR,14 bits |
| 0x100| Resolution/Sample rate override | 0x00 | Default resolution and Sample rate |


## AD9249_Interface IP Configuration and operation 

This IP is package as an IP for use with Vivado IP integrator. It was design using Vivado 2018.1. To add this IP to your design, clone this repository in your favorite IP folder and add it to the IP repository in vivado. The IP can be integrated using a block design or instanciated in the top VHL/Verilog module of your design.The AXI4 interfaces are meant to be connected to a high performance AXI4 slave on the Zynq CPU. A memory space of 128Mb by default is expected in this IP. 

### Pinout 

The pinout of the IP is described in the following table. 

| Pin | type | function |
| --- | ---- | -------- |
| ADC_SC_axi | AXI4-lite slave interface to program Slow Control Registers | reset/enable/configure acquisition |
| ADC0/1_axi | AXI4 memory map master interface to channel 0-7 and 8-15 data | Acquisition of ADC data | 
| ADC_FC_P/N | Input pins for ADC Frame clock, bit 0 for channel 0-7, bit 1 for channel 8-15 | Frame clock for acquisition data | 
| ADC_DC_P/N | Input pins for ADC Data clock, bit 0 for channel 0-7, bit 1 for channel 8-15 | Data clock for acquisition data |
| ADC_DOUTP/N | Input pins for ADC data, one bit per channel | Data input, one PN pair per channel, data are DDR vs data clock | 
| trigger_in | Trigger input | Trigger data acquisition of ADC data stream | 
| clk100m | Clock input | System clock for capturing data from ADC | 
| clk200m | Clock input | Reference clock for IDELAY2 element (clock to data phase adjustment)|
| ADC0/ADC1/ADC_SC_reset | Reset input | Reset for AXI interfaces, can be connected together | 
| ADC0/ADC1/ADC_SC_clk | Clock input | clock input for AXI interface, can be connected together | 


### Clocking of the IP 

##### AXI clocks 

The AXI interfaces can be clocked using the default system clock, typically 200 MHz. This clock will determine the data transfer rate to memory for the data interfaces. The associated reset should be connected to system reset or to a convenient signal.  

##### System and Reference clock

The system clock (DAQ_CLK) should be of the same frequency as the Sampling clock for the ADC. The reference clock (DELAY_REFERENCE_CLK) should be 200 MHz (Otherwise, modify the IDELAY2 IP parameter in the code). On the CaR board, They should be provided by a MMCM synchronized to  the clk input #8 of the Si5345, which also provide the ADC input clock. 

##### Trigger input

The trigger input signal can be used to start a burst of ADC data to the AXI4 interfaces. The length of the trigger pulse should be larger than AXI clock period for reliable results. Trigger is active-high.


### IP Configuration registers

The following table describe the registers of the IP. The registers can be written/read via the ADC_SC aci interface. The address in the table should be offset to correspond to the AXI interface allocated memory space in your design.


| Register | Address | valid bits | function | 
| -------- | ------- | ---------- | -------- |
| Data Generator reset | 0x0 | bit 0 - channel 0-7, bit 1 - channel 8-15 | Reset the data generator to its start value, active high|
| Trigger | 0x4 | bit 0 - channel 0-7, bit 1 - channel 8-15 | Trigger data acquisition via AXI interface,active high |
| FIFO reset | 0x8 | bit 0 - channel 0-7, bit 1 - channel 8-15 | Reset input FIFO capturing data generator or ADC data, active high | 
| Address reset | 0xC | bit 0 - channel 0-7, bit 1 - channel 8-15 | Reset start adress when writing to AXI4 memory mapped interface (ADC0/ADC1_axi), active high | 
| burst enable | 0x10 | bit 0 - channel 0-7, bit 1 - channel 8-15 | Set to 1 to enable memory bursts for data acquisition | 
| Test data enable | 0x14 | bit 0 - enable for all channel | By pass ADC data and enable internal data generator, for debugging | 
| ADC0 burst length | 0x18 | all bits | Length of data acquisition after trigger, in 128 sample increments (0x1 means 128 samples in acquisition | 
| ADC0 Current address | 0x1C | bits 0-5 | Read-only, current address for ADC0 axi interface, next or current data burst to be written at this address | 
| ADC1 burst length | 0x20 | all bits | Length of data acquisition after trigger, in 128 sample increments (0x1 means 128 samples in acquisition | 
| ADC1 Current address | 0x24 | bits 0-5 | Read-only, current address for ADC1 axi interface, next or current data burst to be written at this address | 
| ADC Data Clock Frequency | 0x28 | all bits | Read-only register containing the measured ADC Data clock frequency in MHz (rounded to closest integer) |
| ADC Frame Clock Frequency | 0x2C | all bits | Read-only register containing the measured ADC Frame clock frequency in MHz (rounded to closest integer) |
| ADC Frame-Data alignement | 0x30 | bit 0-3 | Used to align ADC data and frame clock. Each increment adjust the phase per one ADC data clock cycle | 
| ADC Bank Selection | 0x34 | bit 0 | Select between channel 0-7 and 8-15 to configure delays and read clock frequencies, 0 for channel 0-7, 1 for 8-15 |
| Load signal for ADC data clock fine delay correction | 0x34 | bit 6 | Active high, load counter value into the IDELAY2 FW block, affect bank selected by Bank Selection | 
| ADC Data Clock fine delay adjustment | 0x34 | bit 1-5 | Value of the fine delay correction for ADC frame data, increment of one change delay per t = 1/(2*Sampling Clock Frequency), affect bank selected by Bank Selection| 
| ADC Data Clock fine delay adjustment readout | 0x38 | bits 0-4 | Readback of ADC Data clock delay value, for the bank selected by Bank Selection |
| Fine delay stable flag | 0x3C | bit 0 | Read only, bit indicating that fine delay cell for ADC Data Clock is stable, for the bank selected by Bank Selection |




### IP Operation

##### Initialization 

1. Power-up ADC, program SI5345 or other input clock to produce desired input clock
2. Program ADC internal registers using SPI interface 
3. Reset FIFO, AXI Write adresses and Data Generator using their respective registers on the AXI4-lite interface. The FIFO require ~1us to reset properly
4. Program burst length to select the waveform length, in units of 2048b (2kb), or 128 samples
5. Enable Data Generator if desired
6. Enable burst to allow data to be written to memory 
7. Send trigger via slow control or external trigger pin to begin one acquisition


##### Synchronization

In case of misalignement between the data and frame clock of each channel bank, the offset between the two clock can be adjusted by increment of the data clock period. The ADC Frame-Data alignement register can be incremented to modify the offset seen by the data capture circuit. 

To align the internal DAQ clock to the ADC data clock, the ADC data clock is routed to a IDELAY2 primitive (see [documentation, p116](https://www.xilinx.com/support/documentation/user_guides/ug471_7Series_SelectIO.pdf)). The Delay between the two clock can be corrected in increments of 0.078126 ns for a 200MHz reference clock (see [documentation](https://www.xilinx.com/support/documentation/data_sheets/ds187-XC7Z010-XC7Z020-Data-Sheet.pdf) p42). Using the ADC Data Clock fine delay adjustment and its load registers, the delay can be adjusted. 

To facilitate clock alignement, test pattern can be activated in the ADC using the SPI interface register 0x0D. See [documentation](https://www.analog.com/media/en/technical-documentation/data-sheets/AD9249.pdf) for more details.

##### Data Acquisition 

On reception of a trigger, an acquisition corresponding to the burst length programmed is initiated. The ADC Current Address register indicate the next or current start adress in the memory where the acquisition data will be written. Each sample is encoded in 128 bits and written to memory in two consecutive 64 bits words. each channel correspond to 16 bits of the 128 bit word. There is 8 Channel per AXI channel.

for example, if the acquisition starts at address 0x00000080, the first sample consists of the 64 bit word at address 0x80 is {ch0,ch1,ch2,ch3,ch4,ch5,ch6,ch7} and the 64 bit word at address 0x81 with {ch8,ch9,ch10,ch11,ch12,ch13,ch14,ch15}. Next sample follows at address 0x82 and 0x83. 

After each trigger, the starting address is incremented by 0x80 * number of burst selected by programming the register. When the end of the memory is reached (after 128Mb of acquisition by default), the address returns to zero and previous written data are overwritten.


##### Simulation 

A SystemVerilog testbench demonstrating the operation of the IP is available in the ip folder at src/ADCTB.sv. It makes use of the AXI VIP for AXI protocol verification.







